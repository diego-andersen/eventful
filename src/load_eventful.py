#! /usr/bin/env python
"""
Load JSON dump of Eventful data into appropriate database.
"""

import argparse
import os
import sys
import gzip
import json
import psycopg2
from datetime import datetime

def main():
    default_db = '{"host": "localhost", "port": "5432", "db": "wristcontrol", \
                  "usr": "wristcontrol", "pwd": ""}'
    default_path = '/Users/wristcontrol/projects/work/gyana/eventful/data'

    parser = argparse.ArgumentParser(description = "Load Eventful JSON into appropriate database.")

    parser.add_argument('-db', '--database', type = json.loads, nargs = '?',
                        help = 'Database connection parameters.', default = default_db)
    parser.add_argument('-s', '--source', nargs = 1, help = '/path/to/data', default = default_path)
    args = parser.parse_args()

    # Connect to DB
    db = args.database

    try:
        con = psycopg2.connect(host = db['host'], port = db['port'], database = db['db'],
                               user = db['usr'], password = db['pwd'])
    except:
        print("Could not connect to %s.%s" % (db['host'], db['db']))
        sys.exit(0)

    print("Successfully connected to %s.%s" % (db['host'], db['db']))

    # Create schema from scratch if needed
    cursor = con.cursor()
    cursor.execute("CREATE EXTENSION IF NOT EXISTS PostGIS;")
    cursor.execute("CREATE SCHEMA IF NOT EXISTS eventful AUTHORIZATION wristcontrol;")
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS eventful.events
        ( id VARCHAR
        , all_day VARCHAR(1)
        , start_time TIMESTAMP
        , stop_time TIMESTAMP
        , popularity NUMERIC
        , venue_id VARCHAR
        , event_dump JSON
          ) WITH OIDS;
    """)
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS eventful.venues
        ( id VARCHAR
        , name VARCHAR
        , location GEOMETRY(POINT)
        , popularity NUMERIC
        , venue_dump JSON
          ) WITH OIDS;
    """)
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS eventful.performers
        ( id VARCHAR
        , name VARCHAR
        , popularity NUMERIC
        , performer_dump JSON
          ) WITH OIDS;
    """)
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS eventful.load_log
        ( tstamp TIMESTAMP
        , filepath VARCHAR
          ) WITH OIDS;
    """)

    # Find the source file
    file = os.path.join(args.source, 'gyana-20170106-full.json.gz')

    print(file)

    # json.load doesn't take binary input before 3.6
    if sys.version_info[:2] >= (3, 6):
        json_raw = json.load(gzip.open(file, 'rb'))
    else:
        with gzip.open(file, 'rb') as f:
            json_raw = json.loads(f.read().decode('utf-8'))

    # Split JSON into component parts
    j_events = json_raw['events']
    j_venues = json_raw['venues']
    j_performers = json_raw['performers']

    # Insert into events table
    for event in j_events:
        event_id = event['id']
        venue_id = event['venue_id']
        start_time = event['start_time']
        event_popularity = int(event['popularity'])

        if event['stop_time']:
            stop_time = event['stop_time']
        else:
            stop_time = None

        if event['all_day'] == '1':
            all_day = 'Y'
        else:
            all_day = None

        cursor.execute("""
            INSERT INTO eventful.events ( id, all_day, start_time, stop_time
                                        , popularity, venue_id, event_dump )
            VALUES (%s, %s, %s, %s, %s, %s, %s)
            """, [event_id, all_day, start_time, stop_time, event_popularity, venue_id, json.dumps(event)]
        )

    # Insert into venues table
    for venue in j_venues:
        venue_id = venue['id']
        venue_name = venue['name']
        coords = 'POINT (%s %s)' % (venue['longitude'], venue['latitude'])
        venue_popularity = int(venue['popularity'])

        cursor.execute("""
            INSERT INTO eventful.venues (id, name, location, popularity, venue_dump)
            VALUES (%s, %s, ST_GeomFromText(%s, 4326), %s, %s)
            """, [venue_id, venue_name, coords, venue_popularity, json.dumps(venue)]
        )

    # Insert into performers table
    for performer in j_performers:
        performer_id = performer['performer_id']
        performer_name = performer['name']
        performer_popularity = int(performer['popularity'])

        cursor.execute("""
            INSERT INTO eventful.performers (id, name, popularity, performer_dump)
            VALUES (%s, %s, %s, %s)
            """, [performer_id, performer_name, performer_popularity, json.dumps(performer)]
        )

    cursor.execute("""
        INSERT INTO eventful.load_log (tstamp, filepath)
        VALUES (%s, %s)
        """, [datetime.now(), file]
    )

    con.commit()

    cursor.close()
    con.close()

if __name__ == "__main__":
    main()
