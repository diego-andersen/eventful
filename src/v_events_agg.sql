create or replace view eventful.v_events_agg as
select ST_SnapToGrid(e.geom, 0, 0, 0.01, 0.01) as geom
     , date_part('isodow', e.date) as dow
     , date_part('hour', e.date) as hour
     , count(*) as value
  from eventful.v_events e
 group by ST_SnapToGrid(e.geom, 0, 0, 0.01, 0.01)
        , e.date
