create or replace view eventful.v_events_hourly as
select e.date as hour
     , c.centre as geom
     , count(*) as activity
  from eventful.v_events e
  join public.ref_cities c
    on st_within(e.geom, c.envelope)
 group by c.centre, e.date
 order by c.centre, e.date
