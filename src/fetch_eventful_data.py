#! /usr/bin/env python
"""
Load JSON dump of Eventful data into appropriate database.
"""

import argparse
import os
import io
import sys
import http.client
import gzip
import json
import psycopg2
from datetime import datetime

def fetch_json():
    # Basic path for HTTP request
    basepath = datetime.now().strftime('/images/export/gyana-%Y%m%d')
    #basepath = "/images/export/gyana-20170106"

    # Check what feed is available
    conn = http.client.HTTPConnection("static.eventful.com")
    conn.request("HEAD", basepath + "-full.json.gz")
    res = conn.getresponse()
    res.read()

    # Complete path depending on request type - full feed only on Wednesdays
    if res.status == 200:
        fullpath = basepath + "-full.json.gz"
    else:
        fullpath = basepath + "-updates.json.gz"

    conn.request("GET", fullpath)
    res = conn.getresponse()
    compressed_file = io.BytesIO(res.read())
    decompressed_file = gzip.GzipFile(fileobj = compressed_file)

    return json.loads(decompressed_file.read().decode('utf-8')), "http://static.eventful.com" + fullpath

def main():
    # argparse makes troubleshooting easier
    default_db = '{"host": "localhost", "port": "5432", "db": "wristcontrol", \
                  "usr": "wristcontrol", "pwd": ""}'

    parser = argparse.ArgumentParser(description = "Load Eventful JSON into appropriate database.")
    parser.add_argument('-db', '--database', type = json.loads, nargs = '?',
                        help = 'Database connection parameters.', default = default_db)
    args = parser.parse_args()

    # Connect to DB
    db = args.database

    try:
        con = psycopg2.connect(host = db['host'], port = db['port'], database = db['db'],
                               user = db['usr'], password = db['pwd'])
    except:
        print("Could not connect to %s.%s" % (db['host'], db['db']))
        sys.exit(0)

    print("Successfully connected to %s.%s" % (db['host'], db['db']))

    # Create schema from scratch if needed
    cursor = con.cursor()
    cursor.execute("CREATE EXTENSION IF NOT EXISTS PostGIS;")
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS eventful.events
        ( id VARCHAR PRIMARY KEY
        , withdrawn VARCHAR(1)
        , all_day VARCHAR(1)
        , start_time TIMESTAMP
        , stop_time TIMESTAMP
        , popularity NUMERIC
        , venue_id VARCHAR
        , event_dump JSON
          ) WITH OIDS;
    """)
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS eventful.venues
        ( id VARCHAR PRIMARY KEY
        , name VARCHAR
        , location GEOMETRY(POINT)
        , popularity NUMERIC
        , venue_dump JSON
          ) WITH OIDS;
    """)
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS eventful.performers
        ( id VARCHAR PRIMARY KEY
        , name VARCHAR
        , withdrawn VARCHAR(1)
        , popularity NUMERIC
        , performer_dump JSON
          ) WITH OIDS;
    """)
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS eventful.load_log
        ( tstamp TIMESTAMP
        , filepath VARCHAR
          ) WITH OIDS;
    """)

    # Fetch JSON from Eventful website, and its URL
    json_raw, fpath = fetch_json()
    print("Downloaded %s" % fpath)

    # Split JSON into component parts
    j_events = json_raw['events']
    j_venues = json_raw['venues']
    j_performers = json_raw['performers']

    # Update events table
    for event in j_events:
        for item in event['categories']:
            for key in item.keys():
                item[key] = item[key].replace('&amp;', '&')

        event_id = event['id']
        venue_id = event['venue_id']
        start_time = event['start_time']
        event_popularity = int(event['popularity'])

        if event['stop_time']:
            stop_time = event['stop_time']
        else:
            stop_time = None

        if event['all_day'] == '0':
            all_day = None
        else:
            all_day = 'Y'

        if event['withdrawn'] == '0':
            withdrawn = None
        else:
            withdrawn = 'Y'

        # Postgres equivalent of MERGE
        cursor.execute("""
            INSERT INTO eventful.events
            (id, withdrawn, all_day, start_time, stop_time, popularity, venue_id, event_dump)
            VALUES ( %(id)s, %(withdrawn)s, %(all_day)s, %(start)s, %(stop)s
                   , %(pop)s, %(v_id)s, %(dmp)s )
                ON CONFLICT (id) DO UPDATE
               SET (withdrawn, all_day, start_time, stop_time, popularity, venue_id, event_dump)
                   = ( %(withdrawn)s, %(all_day)s, %(start)s, %(stop)s
                     , %(pop)s, %(v_id)s, %(dmp)s)
             WHERE eventful.events.id = %(id)s
            """, {"id": event_id, "withdrawn": withdrawn, "all_day": all_day,
                  "start": start_time, "stop": stop_time, "pop": event_popularity,
                  "v_id": venue_id, "dmp": json.dumps(event)}
        )

    print("Updated events table")

    # Update venues table
    for venue in j_venues:
        venue_id = venue['id']
        venue_name = venue['name']
        coords = 'POINT (%s %s)' % (venue['longitude'], venue['latitude'])
        venue_popularity = int(venue['popularity'])

        cursor.execute("""
            INSERT INTO eventful.venues (id, name, location, popularity, venue_dump)
            VALUES (%(id)s, %(name)s, ST_GeomFromText(%(loc)s, 4326), %(pop)s, %(dmp)s)
                ON CONFLICT (id) DO UPDATE
               SET (name, location, popularity, venue_dump)
                   = (%(name)s, ST_GeomFromText(%(loc)s, 4326), %(pop)s, %(dmp)s)
             WHERE eventful.venues.id = %(id)s
            """, {"id": venue_id, "name": venue_name, "loc": coords,
                  "pop": venue_popularity, "dmp": json.dumps(venue)}
        )

    print("Updated venues table")

    # Update performers table
    for performer in j_performers:
        performer_id = performer['performer_id']
        performer_name = performer['name']
        performer_popularity = int(performer['popularity'])

        if performer['withdrawn'] == '0':
            withdrawn = None
        else:
            withdrawn = 'Y'

        cursor.execute("""
            INSERT INTO eventful.performers (id, name, withdrawn, popularity, performer_dump)
            VALUES (%(id)s, %(name)s, %(withdrawn)s, %(pop)s, %(dmp)s)
                ON CONFLICT (id) DO UPDATE
               SET (name, withdrawn, popularity, performer_dump)
                   = (%(name)s, %(withdrawn)s, %(pop)s, %(dmp)s)
             WHERE eventful.performers.id = %(id)s
            """, {"id": performer_id, "name": performer_name, "withdrawn": withdrawn,
                  "pop": performer_popularity, "dmp": json.dumps(performer)}
        )

    print("Updated performers table")

    # Log the update. Duplicates should show up.
    cursor.execute("""
        INSERT INTO eventful.load_log (tstamp, filepath)
        VALUES (%s, %s)
        """, [datetime.now(), fpath]
    )

    print("Updated log table")

    con.commit()
    print("Changes committed")

    cursor.close()
    con.close()

if __name__ == "__main__":
    main()
