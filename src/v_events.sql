create or replace view eventful.v_events as
select generate_series (date_trunc('hour', e.start_time), e.stop_time - interval '1 minute', interval '1 hour') as date
     , v.location as geom
     , e.popularity
     , string_agg(categories, ', ') as labels
  from ( select start_time
              , case when stop_time is null
                          then case when all_day is not null
                                         then start_time + interval '24 hours'
                                    else start_time + interval '3 hours'
                                end
                     else stop_time
                 end as stop_time
              , popularity
              , venue_id
              , json_array_elements(event_dump->'categories')->>'name' as categories
           from eventful.events
          where withdrawn is null
                ) e
  join eventful.venues v
    on v.id = e.venue_id
 group by e.start_time
        , e.stop_time
        , v.location
        , e.popularity
