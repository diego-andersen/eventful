create or replace view eventful.v_events_fixed as
select generate_series(date_trunc('hour', a.start_time), a.stop_time - interval '1 minute', interval '1 hour') as hour
     , a.location as geom
     , a.popularity
  from ( select case when all_day is not null
                          then date(start_time) + interval '9 hours'
                     else start_time
                 end as start_time
              , case when stop_time is null
                          then case when all_day is not null
                                         then date(start_time) + interval '18 hours'
                                    else start_time + interval '3 hours'
                                end
                     else stop_time
                 end as stop_time
              , v.location
              , e.popularity
           from eventful.events e
           join eventful.venues v
             on e.venue_id = v.id
                ) a
               

